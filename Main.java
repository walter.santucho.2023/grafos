// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        int V = 5;
        Grafo grafo = new Grafo(V);

        grafo.agregarArista(0, 1, 2);
        grafo.agregarArista(0, 2, 4);
        grafo.agregarArista(1, 2, 1);
        grafo.agregarArista(1, 3, 7);
        grafo.agregarArista(2, 4, 3);
        grafo.agregarArista(3, 4, 1);


        grafo.dijkstra(0,4);
        }
}